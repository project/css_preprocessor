<?php

/**
 * @file
 * @todo.
 */

/**
 * @todo.
 */
class CSS_CSSPreprocessor extends CSSPreprocessor {

  public function __construct() {

  }

  public function addStylesheet($css) {

  }
}

/**
 * A CSS parser that builds more restrictive grammar than the core syntax and
 * also tries to keep known CSS-hacks.
 *
 * @see http://www.w3.org/TR/CSS2/syndata.html#syntax
 *   Core CSS syntax
 * @see http://www.w3.org/TR/CSS2/grammar.html#grammar
 *   A more restrictive grammar that is closer to the CSS level 2
 * @see http://www.w3.org/TR/css3-syntax/#detailed-grammar
 *
 * @see http://www.w3.org/TR/CSS2/syndata.html#parsing-errors
 * @see http://www.w3.org/TR/css3-syntax/#error-handling
 */
class CSS_CSSParser extends CSSParser {

  /**
   *
   */
  protected $charset;

  /**
   *
   */
  protected $import;

  /**
   *
   */
  protected $rulesets;

  /**
   *
   * @return unknown_type
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   *
   * @return unknown_type
   */
  protected function next() {

  }

  /**
   *
   * @see CSSParser#parseAtRule()
   */
  protected function parseAtRule() {
    if (($statement = parent::parseAtRule()) === FALSE) {
      return FALSE;
    }

    if ($statement['at-rule'] == '@charset') {

    }
    elseif ($statement['at-rule'] == '@import') {

    }

    return $statement;
  }
}

/**
 * A CSS lexer/tokenizer.
 *
 * Lexical analysis is the process of converting a sequence of characters into a
 * sequence of tokens.
 */
class CSSLexer {

  /**
   * The list of tokens for CSS.
   *
   * @see http://www.w3.org/TR/CSS2/syndata.html#tokenization
   */
  const T_IDENT         = 1;  // {ident}
  const T_ATKEYWORD     = 2;  // @{ident}
  const T_STRING        = 3;  // {string}
  const T_INVALID       = 4;  // {invalid}
  const T_HASH          = 5;  // #{name}
  const T_NUMBER        = 6;  // {num}
  const T_PERCENTAGE    = 7;  // {num}%
  const T_DIMENSION     = 8;  // {num}{ident}
  const T_URI           = 9;  // url\({w}{string}{w}\)
                              // |url\({w}([!#$%&*-~]|{nonascii}|{escape})*{w}\)
  const T_UNICODE_RANGE = 10; // u\+[0-9a-f?]{1,6}(-[0-9a-f]{1,6})?
  const T_CDO           = 11; // <!--
  const T_CDC           = 12; // -->

  const T_COLON         = 13; // :
  const T_SEMICOLON     = 14; // ;
  const T_CURLY_OPEN    = 15; // {
  const T_CURLY_CLOSE   = 16; // }
  const T_ROUND_OPEN    = 17; // (
  const T_ROUND_CLOSE   = 18; // )
  const T_SQUARE_OPEN   = 19; // [
  const T_SQUARE_CLOSE  = 20; // ]

  const T_S             = 21; // [ \t\r\n\f]+
  const T_COMMENT       = 22; // \/\*[^*]*\*+([^/*][^*]*\*+)*\/
  const T_FUNCTION      = 23; // {ident}\(
  const T_INCLUDES      = 24; // ~=
  const T_DASHMATCH     = 25; // |=
  const T_DELIM         = 26; // any other character not matched by the above
                              // rules, and neither a single nor a double quote

  /**
   * Contains the regular expressions for all CSS tokens.
   *
   * For readability the regular expressions contains macros that are replaced
   * at runtime. The full regular expressions are generated at construction of
   * the first instance of this class.
   */
  protected static $tokenDefinitions = array(
    // T_URI, T_UNICODE_RANGE and T_FUNCTION should precede T_IDENT
    self::T_URI           => 'url\({w}{string}{w}\)|url\({w}([!#$%&*-~]|{nonascii}|{escape})*{w}\)',
    self::T_UNICODE_RANGE => 'u\+[0-9a-f?]{1,6}(-[0-9a-f]{1,6})?',
    self::T_FUNCTION      => '{ident}\(',

    self::T_IDENT         => '{ident}',
    self::T_ATKEYWORD     => '@{ident}',
    self::T_STRING        => '{string}',
    self::T_INVALID       => '{invalid}',
    self::T_HASH          => '#{name}',

    // T_PERCENTAGE and T_DIMENSION should precede T_NUMBER
    self::T_PERCENTAGE    => '{num}%',
    self::T_DIMENSION     => '{num}{ident}',
    self::T_NUMBER        => '{num}',


    self::T_CDO           => '\<\!\-\-',
    self::T_CDC           => '\-\-\>',

    self::T_COLON         => '\:',
    self::T_SEMICOLON     => ';',
    self::T_CURLY_OPEN    => '\{',
    self::T_CURLY_CLOSE   => '\}',
    self::T_ROUND_OPEN    => '\(',
    self::T_ROUND_CLOSE   => '\)',
    self::T_SQUARE_OPEN   => '\[',
    self::T_SQUARE_CLOSE  => '\]',

    self::T_S             => '[ \t\r\n\f]+',
    self::T_COMMENT       => '\/\*[^*]*\*+([^/*][^*]*\*+)*\/',
    self::T_INCLUDES      => '~\=',
    self::T_DASHMATCH     => '\|\=',
    self::T_DELIM         => '[^\'"]',
  );

  /**
   * A list of macros to be reused in the regular expressions of $tokenList.
   */
  protected static $macroDefinitions = array(
    'ident'               => '[-]?{nmstart}{nmchar}*',
    'name'                => '{nmchar}+',
    'nmstart'             => '[_a-z]|{nonascii}|{escape}',
    'nonascii'            => '[^\0-\177]',
    'unicode'             => '\\\\[0-9a-f]{1,6}(\r\n|[ \n\r\t\f])?',
    'escape'              => '{unicode}|\\\\[^\n\r\f0-9a-f]',
    'nmchar'              => '[_a-z0-9-]|{nonascii}|{escape}',
    'num'                 => '[0-9]+|[0-9]*\.[0-9]+',
    'string'              => '{string1}|{string2}',
    'string1'             => '"([^\n\r\f\\\\"]|\\\\{nl}|{escape})*"',
    'string2'             => '\'([^\n\r\f\\\\\']|\\\\{nl}|{escape})*\'',
    'invalid'             => '{invalid1}|{invalid2}',
    'invalid1'            => '\"([^\n\r\f\\\\"]|{nl}|{escape})*',
    'invalid2'            => '\'([^\n\r\f\\\\\']|{nl}|{escape})*',
    'nl'                  => '\n|\r\n|\r|\f',
    'w'                   => '[ \t\r\n\f]*',
  );

  /**
   * Holds the generated regular expressions of any CSS token.
   */
  protected static $tokenList;

  /**
   * Contains the unparsed input CSS.
   */
  protected $css;

  /**
   * List of parsed tokens.
   *
   * Every token has the following keys:
   * - 'token': The token type. See defined constants in this class beginning
   *   with T_ for allowed values.
   * - 'value': The token value that is matched by the regular expression of
   *   the token type.
   */
  protected $tokens = array();

  /**
   * Constructor.
   *
   * @param $css
   *   Input CSS to be tokenized.
   */
  public function __construct($css) {
    $this->css = $css;
    $this->tokens = array();

    if (!isset(self::$tokenList)) {
      // Only generate the regular expressions once.
      foreach (self::$tokenDefinitions as $type => $regexp) {
        // Replace macros.
        $regexp = preg_replace_callback('/\{([a-z0-9]+)\}/',
                                        array($this, '_replaceMacro'), $regexp);
        // Assert to start of unparsed input CSS.
        self::$tokenList[$type] = '`^(?:' . $regexp . ')`i';
      }
    }

    // Parse first token.
    $this->next();
    reset($this->tokens);
  }

  /**
   * Helper function to generate the regular expressions.
   */
  protected function _replaceMacro($matches) {
    $macro = self::$macroDefinitions[$matches[1]];
    // Recursivly replace macros.
    $macro = preg_replace_callback('/\{([a-z0-9]+)\}/',
                                   array($this, '_replaceMacro'), $macro);
    // Wrap macro in a non-capturing subpattern.
    return '(?:' . $macro . ')';
  }

  /**
   * Return all tokens.
   *
   * @return
   *   An array containing all tokens.
   */
  public function getAllTokens() {
    $flag = key($this->tokens);
    while ($this->next() !== FALSE);
    $this->seek($flag);

    return $this->tokens;
  }

  /**
   * Return the current token.
   *
   * @return
   *   Array of the current token.
   */
  public function current() {
    return current($this->tokens);
  }

  /**
   * Return the key of the current token.
   *
   * @return
   *   Location of the current token in the token queue.
   */
  public function key() {
    return key($this->tokens);
  }

  /**
   * Determine whether the current token is the last.
   *
   * @return
   *   TRUE if current token is last, FALSE otherwise.
   */
  public function last() {
    if ($this->next() !== FALSE) {
      prev($this->tokens);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Seek to a position.
   *
   * @param $position
   *   Position to seek.
   * @return
   *   Array of the token at given position or FALSE if position does not exist.
   */
  public function seek($position = 0) {
    // @todo cleanup.
    if ($position == 0) {
      reset($this->tokens);
    }
    // If the internal pointer of $this->tokens points beyond the end of the
    // elements list or the array is empty, key() returns NULL.
    elseif (key($this->tokens) === NULL) {
      throw new Exception('key($this->tokens) === NULL');
    }
    elseif ($position > key($this->tokens)) {
      while ($this->next() !== FALSE && key($this->tokens) != $position);
    }
    elseif ($position < key($this->tokens)) {
      if (key($this->tokens) === NULL) end($this->tokens);
      while (prev($this->tokens) !== FALSE && key($this->tokens) != $position);
    }
    return current($this->tokens);
  }

  /**
   * Advance to next token.
   *
   * @return
   *   Array of next token or FALSE if there is no next token.
   */
  public function next() {
    if (next($this->tokens) !== FALSE) {
      // Next token is already been parsed.
      return current($this->tokens);
    }

    if (empty($this->css)) {
      // We have parsed all input CSS.
      return FALSE;
    }

    // Try regular expression of every token type until one matches.
    // @todo As in Lex, in case of multiple matches, the longest match determines the token.
    foreach (self::$tokenList as $type => $regexp) {
      if (preg_match($regexp, $this->css, $matches)) {
        // Add matched token to the token queue.
        $this->tokens[] = array(
          'type' => 'token',
          'token' => $type,
          'value' => $matches[0]
        );

        // Strip matched token from input CSS.
        $this->css = substr($this->css, strlen($matches[0]));

        return current($this->tokens);
      }
    }

    throw new Exception('Malformed CSS');
  }
}

/**
 *
 * @see http://www.w3.org/TR/CSS2/syndata.html#syntax
 *   Core CSS syntax
 * @see http://www.w3.org/TR/CSS2/grammar.html#grammar
 *   A more restrictive grammar that is closer to the CSS level 2
 * @see http://www.w3.org/TR/css3-syntax/#detailed-grammar
 */
class CSSParser {

  /**
   * Holds an instance of CSSLexer.
   *
   * @var CSSLexer
   */
  protected $tokens;

  /**
   * A structured array the represents the input CSS.
   *
   * @var array
   */
  protected $grammar = array();

  /**
   * Constructor.
   *
   * @param $css
   *   Input CSS to be parsed.
   */
  public function __construct($css) {
    $this->tokens = new CSSLexer($css);
    $this->grammar = array();
  }

  /**
   * Parse the input CSS and build the syntax tree.
   */
  public function parse() {
    $this->grammar = $this->parseStylesheet();
  }

  /**
   * Return the CSSLexer.
   *
   * @return CSSLexer
   */
  public function getLexer() {
    return $this->tokens;
  }

  /**
   * Return the grammar.
   *
   * @return
   *   The grammar array.
   */
  public function getGrammar() {
    return $this->grammar;
  }

  /**
   * Helper function: Return the current token.
   *
   * @return
   *   Array of the current token.
   */
  protected function current() {
    return $this->tokens->current();
  }

  /**
   * Helper function: Return the key of the current token.
   *
   * @return
   *   Location of the current token in the token queue.
   */
  protected function key() {
    return $this->tokens->key();
  }

  /**
   * Helper function: Determines whether the current token is the last.
   *
   * @return
   *   TRUE if current token is last, FALSE otherwise.
   */
  protected function last() {
    if (!$this->tokens->last()) {
      $this->skipTokens();
    }
    return $this->tokens->last();
  }

  /**
   * Helper function: restore the internal pointer of $tokens to a given position.
   *
   * @param $flag
   *   Position to restore the internal pointer of $tokens to.
   * @return
   *   FALSE, so parse functions can use this in-line with a return statement.
   */
  protected function restore($flag) {
    $this->tokens->seek($flag);
    $this->skipTokens();

    return FALSE;
  }

  /**
   * Helper function: advance the internal pointer of $tokens.
   *
   * @return
   *   Array of next token or FALSE if there is no next token.
   */
  protected function next() {
    $this->tokens->next();
    $this->skipTokens();

    return $this->tokens->current();
  }

  /**
   * Helper function: skip certain tokens.
   *
   * As per http://www.w3.org/TR/CSS2/syndata.html#comment:
   *   COMMENT tokens do not occur in the grammar (to keep it readable), but any
   *   number of these tokens may appear anywhere outside other tokens.
   *
   * By default comments are not included in the grammar, derived classes may
   * however override this.
   */
  protected function skipTokens() {
    while ($this->matchToken(CSSLexer::T_COMMENT) && $this->tokens->next() !== FALSE);
  }

  /**
   * Helper function: skip whitespace tokens.
   */
  protected function skipWhitespace() {
    while ($this->matchToken(CSSLexer::T_S) && $this->next() !== FALSE);
  }

  /**
   * Helper function: determine if current token matches one of given tokens.
   *
   * Example usage:
   *
   *   $this->matchToken(CSSLexer::T_CDO, CSSLexer::T_CDC, CSSLexer::T_S);
   *
   * @param $token
   *   Token to match. Use T_* constants from the CSSLexer class.
   * @return
   *   Return TRUE if current token matches one of given tokens; FALSE otherwise.
   */
  protected function matchToken() {
    $tokens = func_get_args();
    $token = $this->current();

    return in_array($token['token'], $tokens);
  }

  /**
   * Parse the CSS stylesheet.
   *
   * 1
   * [ CDO | CDC | S | statement ]*;
   *
   * @return
   *   Stylesheet array
   */
  protected function parseStylesheet() {
    $stylesheet = array();
    // 1: [ CDO | CDC | S | statement ]*
    while (!$this->last()) {
      if ($this->matchToken(CSSLexer::T_CDO, CSSLexer::T_CDC, CSSLexer::T_S)) {
        if (!$this->next()) {
          break;
        }
      }
      elseif(($child = $this->parseStatement()) !== FALSE) {
        // Add statement to the stylesheet grammar.
        $stylesheet[] = $child;
      }
      else {
        $token = $this->current();
        throw new Exception('Malformed stylesheet ' . $token['value']);
      }
    }

    return $stylesheet;
  }

  /**
   * Parse a CSS statement.
   *
   * 1
   * ruleset | at-rule;
   *
   * @return
   *   Statement array or FALSE when none is found.
   */
  protected function parseStatement() {
    $statement = array();
    // 1: ruleset | at-rule
    if (($child = $this->parseRuleset()) !== FALSE ||
        ($child = $this->parseAtRule()) !== FALSE) {
      $statement = $child;
    }
    else {
      return FALSE;
    }

    return $statement;
  }

  /**
   * Parse a CSS at-rule.
   *
   * 1         2  3    4
   * ATKEYWORD S* any* [ block | ';' S* ];
   *
   * @return
   *   Statement array or FALSE when none is found.
   */
  protected function parseAtRule() {
    $flag = $this->key();
    $statement = array(
      'type' => 'at-rule'
    );

    // 1: ATKEYWORD
    if (!$this->matchToken(CSSLexer::T_ATKEYWORD)) {
      return $this->restore($flag);
    }
    $statement['at-rule'] = $this->current();
    $this->next();
    // 2: S*
    $this->skipWhitespace();
    // 3: any*
    while (($child = $this->parseAny()) !== FALSE) {
      $statement['options'][] = $child;
    }
    // 4: [ block | ';' S* ]
    if (($child = $this->parseBlock()) !== FALSE) {
      $statement['block'] = $child;
    }
    elseif ($this->matchToken(CSSLexer::T_SEMICOLON)) {
      $this->next();
      $this->skipWhitespace();
    }
    else {
      return $this->restore($flag);
    }

    return $statement;
  }

  /**
   * Parse a CSS ruleset.
   *
   * 1          2  3  4            5                        6   7
   * selector? '{' S* declaration? [ ';' S* declaration? ]* '}' S*;
   *
   * @return
   *   A ruleset array or FALSE when none is found.
   *   - 'type': ruleset
   *   - 'selector': (optional)
   *   - 'declarations':
   */
  protected function parseRuleset() {
    $flag = $this->key();
    $statement = array(
      'type' => 'ruleset'
    );

    // 1: selector?
    if (($selector = $this->parseSelector()) !== FALSE) {
      $statement['selector'] = $selector;
    }
    // 2: {
    if (!$this->matchToken(CSSLexer::T_CURLY_OPEN)) {
      return $this->restore($flag);
    }
    $this->next();
    // 3: S*
    $this->skipWhitespace();
    // 4: declaration?
    if (($declaration = $this->parseDeclaration()) !== FALSE) {
      $statement['declarations'][] = $declaration;
    }
    // 5: [ ';' S* declaration? ]*
    while (!$this->last()) {
      // ;
      if (!$this->matchToken(CSSLexer::T_SEMICOLON)) {
        break;
      }
      $this->next();
      $this->skipWhitespace();
      // Any declaration.
      if (($declaration = $this->parseDeclaration()) !== FALSE) {
        $statement['declarations'][] = $declaration;
      }
    }
    // 6: }
    if (!$this->matchToken(CSSLexer::T_CURLY_CLOSE)) {
      return $this->restore($flag);
    }
    $this->next();
    // 7: S*
    $this->skipWhitespace();

    return $statement;
  }

  /**
   * Parse a CSS block.
   *
   * 1   2  3                                        4   5
   * '{' S* [ any | block | ATKEYWORD S* | ';' S* ]* '}' S*;
   *
   * @return
   *   A block array or FALSE when none is found.
   */
  protected function parseBlock() {
    $flag = $this->key();
    $block = array();

    // 1: {
    if (!$this->matchToken(CSSLexer::T_CURLY_OPEN)) {
      return $this->restore($flag);
    }
    $this->next();
    // 2: S*
    $this->skipWhitespace();
    // 3: [ any | block | ATKEYWORD S* | ';' S* ]*
    while (!$this->last()) {
      if (($child = $this->parseAny()) !== FALSE ||
          ($child = $this->parseBlock()) !== FALSE) {
        $block[] = $child;
      }
      elseif ($this->matchToken(CSSLexer::T_ATKEYWORD)) {
        $block[] = $this->current();
        $this->next();
        $this->skipWhitespace();
      }
      elseif ($this->matchToken(CSSLexer::T_SEMICOLON)) {
        $this->next();
        $this->skipWhitespace();
      }
      else {
        break;
      }
    }
    // 4: }
    if (!$this->matchToken(CSSLexer::T_CURLY_CLOSE)) {
      return $this->restore($flag);
    }
    $this->next();
    // 5: S*
    $this->skipWhitespace();

    return $block;
  }

  /**
   * Parse a CSS selector.
   *
   * 1
   * any+;
   *
   * @return
   *   A selector array or FALSE when none is found.
   */
  protected function parseSelector() {
    $selector = array();

    // 1: any+
    if (($child = $this->parseAny()) === FALSE) {
      return FALSE;
    }
    $selector[] = $child;
    while (($child = $this->parseAny()) !== FALSE) {
      $selector[] = $child;
    }

    return $selector;
  }

  /**
   * Parse a CSS declaration.
   *
   * 1        2  3   4  5
   * property S* ':' S* value;
   *
   * @return
   *   A declaration array or FALSE when none is found.
   */
  protected function parseDeclaration() {
    $flag = $this->key();
    $declaration = array('type' => 'declaration');

    // 1: any+
    if (($child = $this->parseProperty()) === FALSE) {
      return FALSE;
    }
    $declaration['property']= $child;
    // 2: S*
    $this->skipWhitespace();
    // 3: ':'
    if (!$this->matchToken(CSSLexer::T_COLON)) {
      return $this->restore($flag);
    }
    $this->next();
    // 4: S*
    $this->skipWhitespace();
    // 5: value
    if (($child = $this->parseValue()) === FALSE) {
      return $this->restore($flag);
    }
    $declaration['value']= $child;

    return $declaration;
  }

  /**
   * Parse a CSS property.
   *
   * 1
   * IDENT;
   *
   * @return
   *   A property array or FALSE when none is found.
   */
  protected function parseProperty() {
    $property = array();

    // 1: IDENT
    if (!$this->matchToken(CSSLexer::T_IDENT)) {
      return FALSE;
    }
    // Use token array as property array
    $property = $this->current();
    $property['type'] = 'property';
    $this->next();

    return $property;
  }

  /**
   * Parse a CSS value.
   *
   * [ any | block | ATKEYWORD S* ]+;
   *
   * @return
   *   A value array or FALSE when none is found.
   */
  protected function parseValue() {
    $flag = $this->key();
    $value = array();

    // 1: [ any | block | ATKEYWORD S* ]+;
    $first = TRUE;
    while (!$this->last()) {
      if (($child = $this->parseAny()) !== FALSE ||
          ($child = $this->parseBlock()) !== FALSE) {
        $value[] = $child;
      }
      elseif ($this->matchToken(CSSLexer::T_ATKEYWORD)) {
        $value[] = $this->current();
        $this->next();
        $this->skipWhitespace();
      }
      elseif ($first) {
        return $this->restore($flag);
      }
      else {
        break;
      }
      $first = FALSE;
    }

    return $value;
  }

  /**
   * Parse a set of tokens.
   *
   * 1
   * [ IDENT | NUMBER | PERCENTAGE | DIMENSION | STRING | DELIM
   * | URI | HASH | UNICODE-RANGE | INCLUDES | DASHMATCH | ':'
   * 2                        3                                   4
   * | FUNCTION S* any* ')' | '(' S* any* ')' | '[' S* any* ']' ] S*;
   *
   * @return
   *   An any array or FALSE when none is found.
   */
  protected function parseAny() {
    $flag = $this->key();
    $any = array('type' => 'any');

    // 1: [ IDENT | NUMBER | PERCENTAGE | DIMENSION | STRING | DELIM
    //    | URI | HASH | UNICODE-RANGE | INCLUDES | DASHMATCH | ':'
    if ($this->matchToken(CSSLexer::T_IDENT) ||
        $this->matchToken(CSSLexer::T_NUMBER) ||
        $this->matchToken(CSSLexer::T_PERCENTAGE) ||
        $this->matchToken(CSSLexer::T_DIMENSION) ||
        $this->matchToken(CSSLexer::T_STRING) ||
        $this->matchToken(CSSLexer::T_DELIM) ||
        $this->matchToken(CSSLexer::T_URI) ||
        $this->matchToken(CSSLexer::T_HASH) ||
        $this->matchToken(CSSLexer::T_UNICODE_RANGE) ||
        $this->matchToken(CSSLexer::T_INCLUDES) ||
        $this->matchToken(CSSLexer::T_DASHMATCH) ||
        $this->matchToken(CSSLexer::T_COLON)) {
      // Use token array
      $any = $this->current();
      $any['type'] = 'any';
      $this->next();
    }
    // 2: FUNCTION S* any* ')'
    elseif ($this->matchToken(CSSLexer::T_FUNCTION)) {
      // Special case
      // @todo document
      $any['type'] = 'function';
      $any['value'] = $this->current();
      $this->next();
      $this->skipWhitespace();
      while (($child = $this->parseAny()) !== FALSE) {
        $any['children'][] = $child;
      }
      if (!$this->matchToken(CSSLexer::T_ROUND_CLOSE)) {
        return $this->restore($flag);
      }
      // No need to add close token to grammar
      //$any['children'][] = $this->current();
      $this->next();
    }
    // 3: '(' S* any* ')' | '[' S* any* ']'
    elseif ($this->matchToken(CSSLexer::T_ROUND_OPEN, CSSLexer::T_SQUARE_OPEN)) {
      $any['type'] = 'box';
      $any['value'] = $open = $this->current();
      $this->next();
      $this->skipWhitespace();
      while (($child = $this->parseAny()) !== FALSE) {
        $any['children'][] = $child;
      }
      // Close-token always is the next number after the open-token of its type.
      if (!$this->matchToken($open['token'] + 1)) {
        return $this->restore($flag);
      }
      $this->next();
    }
    else {
      return $this->restore($flag);
    }
    // 4: S*
    $this->skipWhitespace();

    return $any;
  }
}

/**
 * CSS preprocessor
 */
class CSSPreprocessor {

  protected $parser;

  public function __construct($css) {
    $this->parser = new CSSParser;
  }

  /**
   * Return the output of the preprocessor.
   *
   * @return
   *   Output of the preprocessor.
   */
  public function getOutput() {
    $css = '';

    $this->parser->parse();
    foreach ($this->parser->getGrammar() as $subset) {

    }

    return $css;
  }
}
